<?php
class Digit
{
    private $number;
    private static $roman = array('I','V','X','L','C','D','M','','');

    public function __construct($number, $position)
    {
        $this->number = $number;
        $this->position = $position;
    }

    public function getText()
    {
        $prefix = $this->getPrefix();
        $mainPart = $this->getMainPart();
        $units = $this->getUnits();
        if($this->number > 4 && $this->number < 9) {
            $nextPart = (new Digit($this->number-5,$this->position))->getText();
        }
        else {
            $nextPart = '';
        }
        return $prefix.$mainPart.$units.$nextPart;
    }
    public function getPrefix()
    {
        $small = self::$roman[$this->position*2];
        if($this->number == 4 || $this->number == 9) {
            return $small;
        }
        else {
            return '';
        }
    }
    public function getMainPart()
    {
        $big = self::$roman[$this->position*2+1];
        $biggest = self::$roman[$this->position*2+2];
        if($this->number > 3 && $this->number < 9) {
            return $big;
        }
        else if($this->number == 9) {
            return $biggest;
        }
        else {
            return '';
        }
    }
    public function getUnits()
    {
        $small = self::$roman[$this->position*2];
        if ($this->number < 4) {
            return str_repeat($small, $this->number);
        }
        else {
            return '';
        }
    }

}