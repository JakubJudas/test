<?php class RomanNumeral
{
    private $number;

    public function __construct($number)
    {
        $this->number = $number;
    }

    public function getText()
    {
        $units = new Digit($this->number % 10, 0);
        $tens = new Digit(($this->number / 10) % 10,1);
        $hundreds = new Digit(($this->number / 100) % 10,2);
        $thousands = new Digit(($this->number / 1000) % 10,3);
        return $thousands->getText() . $hundreds->getText() . $tens->getText() . $units->getText();
    }


}
