<?php
class RomanNumeralTest extends PHPUnit_Framework_TestCase
{
    public function testRomanNumerals()
    {
        $number1 = new RomanNumeral(1);
        $number99 = new RomanNumeral(99);
        $number101 = new RomanNumeral(101);
        $number1001 = new RomanNumeral(1001);
        $this->assertEquals('I',$number1->getText());
        $this->assertEquals('XCIX',$number99->getText());
        $this->assertEquals('CI',$number101->getText());
        $this->assertEquals('MI',$number1001->getText());

    }
}