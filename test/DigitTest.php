<?php
class DigitTest extends PHPUnit_Framework_TestCase
{
    public function testGetSingles()
    {
        $digit = new Digit(1, 0);
        $digit2 = new Digit(2, 0);
        $digit4 = new Digit(4, 0);
        $digit5 = new Digit(5, 0);
        $digit6 = new Digit(6, 0);

        $this->assertEquals('I', $digit->getText());
        $this->assertEquals('II', $digit2->getText());
        $this->assertEquals('IV', $digit4->getText());
        $this->assertEquals('V', $digit5->getText());
        $this->assertEquals('VI', $digit6->getText());
    }

    public function testGetDecimals()
    {
        $digit = new Digit(1, 1);
        $digit2 = new Digit(2, 1);
        $digit4 = new Digit(4, 1);
        $digit5 = new Digit(5, 1);
        $digit6 = new Digit(6, 1);

        $this->assertEquals('X', $digit->getText());
        $this->assertEquals('XX', $digit2->getText());
        $this->assertEquals('XL', $digit4->getText());
        $this->assertEquals('L', $digit5->getText());
        $this->assertEquals('LX', $digit6->getText());
    }

    public function testGetHundreds()
    {
        $digit = new Digit(1, 2);
        $digit2 = new Digit(2, 2);
        $digit4 = new Digit(4, 2);
        $digit5 = new Digit(5, 2);
        $digit6 = new Digit(6, 2);

        $this->assertEquals('C', $digit->getText());
        $this->assertEquals('CC', $digit2->getText());
        $this->assertEquals('CD', $digit4->getText());
        $this->assertEquals('D', $digit5->getText());
        $this->assertEquals('DC', $digit6->getText());
    }

    public function testGetThousands()
    {
        $digit = new Digit(1, 3);

        $this->assertEquals('M', $digit->getText());
    }
    public function testGetPrefix()
    {
        $digit4 = new Digit(4,0);
        $digit5 = new Digit(5,0);
        $this->assertEquals('I', $digit4->getPrefix());
        $this->assertEquals('', $digit5->getPrefix());
    }
    public function testGetUnits()
    {
        $digit1 = new Digit(1,0);
        $digit2 = new Digit(2,0);
        $digit4 = new Digit(4,0);
        $digit5 = new Digit(5,0);
        $this->assertEquals('I', $digit1->getUnits());
        $this->assertEquals('II', $digit2->getUnits());

        $this->assertEquals('', $digit4->getUnits());
        $this->assertEquals('', $digit5->getUnits());
    }
    public function testGetMainPart()
    {
        $digit5 = new Digit(5,0);
        $digit9 = new Digit(9,0);
        $this->assertEquals('V', $digit5->getMainPart());
        $this->assertEquals('X', $digit9->getMainPart());


    }
}